package com.experis.service.commons.model.dto;

import java.io.Serializable;

/**
 * Class for job information DTO
 * @author dpanquev
 * @version 2021-06-21
 * */
public class SearchProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private String brand;

    private int priceIni;

    private int priceFin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPriceIni() {
        return priceIni;
    }

    public void setPriceIni(int priceIni) {
        this.priceIni = priceIni;
    }

    public int getPriceFin() {
        return priceFin;
    }

    public void setPriceFin(int priceFin) {
        this.priceFin = priceFin;
    }
}
