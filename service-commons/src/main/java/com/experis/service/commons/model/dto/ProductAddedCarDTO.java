package com.experis.service.commons.model.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Class for job information DTO
 * @author dpanquev
 * @version 2021-06-21
 * */
public class ProductAddedCarDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String brand;

    private Date create_at;

    private Long totalPrice;

    private Long quantity;

    public ProductAddedCarDTO(Long id, String name, String brand, Date create_at, Long totalPrice, Long quantity) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.create_at = create_at;
        this.totalPrice = totalPrice;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Date getCreate_at() {
        return create_at;
    }

    public void setCreate_at(Date create_at) {
        this.create_at = create_at;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
