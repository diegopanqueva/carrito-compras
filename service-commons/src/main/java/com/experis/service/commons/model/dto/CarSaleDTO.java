package com.experis.service.commons.model.dto;

import java.io.Serializable;

/**
 * Class for job information DTO
 * @author dpanquev
 * @version 2021-06-21
 * */
public class CarSaleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long idProduct;

    private Long quantity;

    private Long unitPrice;

    private String snSale;

    private String nameUser;

    public CarSaleDTO(Long idProduct, Long quantity, Long unitPrice, String snSale, String nameUser) {
        this.idProduct = idProduct;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.snSale = snSale;
        this.nameUser = nameUser;
    }

    public CarSaleDTO() {
    }

    public Long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Long idProduct) {
        this.idProduct = idProduct;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getSnSale() {
        return snSale;
    }

    public void setSnSale(String snSale) {
        this.snSale = snSale;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
