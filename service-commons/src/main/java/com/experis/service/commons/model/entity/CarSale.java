package com.experis.service.commons.model.entity;



import com.experis.service.commons.model.dto.CarSaleDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Class for job with the entities h2 database
 * @author dpanquev
 * @version 2021-06-21
 * */
@Entity
public class CarSale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long idProduct;

    private Long quantity;

    private Long unitPrice;

    private String snSale;

    private String nameUser;

    private Date create_at;

    private Long totalPrice;

    public CarSale(Long id, Long idProduct, Long quantity, Long unitPrice, String snSale, String nameUser, Date create_at, Long totalPrice) {
        this.id = id;
        this.idProduct = idProduct;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.snSale = snSale;
        this.nameUser = nameUser;
        this.create_at = create_at;
        this.totalPrice = totalPrice;
    }

    public CarSale(CarSaleDTO carSaleDTO) {
        this.id = carSaleDTO.getId();
        this.idProduct = carSaleDTO.getIdProduct();
        this.quantity = carSaleDTO.getQuantity();
        this.unitPrice = carSaleDTO.getUnitPrice();
        this.snSale = carSaleDTO.getSnSale();
        this.nameUser = carSaleDTO.getNameUser();
        this.create_at = new Date();
        this.totalPrice = carSaleDTO.getUnitPrice() * carSaleDTO.getQuantity();
    }

    public CarSale() {
    }

    public Long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Long idProduct) {
        this.idProduct = idProduct;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getSnSale() {
        return snSale;
    }

    public void setSnSale(String snSale) {
        this.snSale = snSale;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public Date getCreate_at() {
        return create_at;
    }

    public void setCreate_at(Date create_at) {
        this.create_at = create_at;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
