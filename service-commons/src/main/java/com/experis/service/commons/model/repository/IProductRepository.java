package com.experis.service.commons.model.repository;


import com.experis.service.commons.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Class for job with all transactions h2 database
 * @author dpanquev
 * @version 2021-06-21
 * */
@Repository
public interface IProductRepository extends JpaRepository<Product, Long> {

    public Product findByNameAndBrand(String name, String brand);

    @Query("select p from Product p where p.name like %?1%")
    public List<Product> findByName(String name);

    public List<Product> findByBrand(String brand);

    public List<Product> findByPriceBetween(int priceIni, int priceFin);
}
