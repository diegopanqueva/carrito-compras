package com.experis.service.commons.model.repository;

import com.experis.service.commons.model.entity.CarSale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Class for job with all transactions h2 database
 * @author dpanquev
 * @version 2021-06-21
 * */
@Repository
public interface ICarSaleRepository extends JpaRepository<CarSale, Long> {

    public CarSale findByIdProductAndSnSaleAndNameUser(Long idProduct, String snSale, String nameUser);

    public List<CarSale> findBySnSaleAndNameUser(String snSale, String nameUser);


}
