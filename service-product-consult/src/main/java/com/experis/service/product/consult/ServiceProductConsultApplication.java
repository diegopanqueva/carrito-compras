package com.experis.service.product.consult;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan({"com.experis.service.commons.model.entity"})
@EnableJpaRepositories("com.experis.service.commons.model.repository")
public class ServiceProductConsultApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceProductConsultApplication.class, args);
	}

}
