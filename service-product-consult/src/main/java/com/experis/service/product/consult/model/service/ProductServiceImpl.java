package com.experis.service.product.consult.model.service;

import com.experis.service.commons.model.dto.ProductAddedCarDTO;
import com.experis.service.commons.model.dto.SearchProductDTO;
import com.experis.service.commons.model.entity.CarSale;
import com.experis.service.commons.model.entity.Product;
import com.experis.service.commons.model.repository.ICarSaleRepository;
import com.experis.service.commons.model.repository.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Interface for job with all consults of shopping car
 * @author dpanquev
 * @version 2021-06-21
 * */
@Service
public class ProductServiceImpl implements IProductService {


    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private ICarSaleRepository carSaleRepository;

    /**
     * Method consult products paged to shopping car
     * @param pageable
     * @return Page<Product> Products paged
     * */
    @Override
    public Page<Product> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    /**
     * Method consult products paged to shopping car
     * @return Page<Product> All Products
     * */
    @Override
    public List<Product> finAll() {
        return productRepository.findAll();
    }

    /**
     * Method consult all Products about advanced search
     * @param searchProductDTO
     * @return List<Product> all Products about advanced search
     * */
    @Override
    public List<Product> search(SearchProductDTO searchProductDTO) {
        List<Product> products = new ArrayList<>();
        List<Product> productsClear = new ArrayList<>();
        if (searchProductDTO.getBrand() != null && searchProductDTO.getBrand() != "") {
            List<Product> lstProdBrand = productRepository.findByBrand(searchProductDTO.getBrand());
            lstProdBrand.forEach(pb -> {
                Product prb = new Product(pb);
                products.add(prb);
            });
        }
        if (searchProductDTO.getName() != null && searchProductDTO.getName() != "") {
            List<Product> lstProdBrand = productRepository.findByName(searchProductDTO.getName());
            lstProdBrand.forEach(pb -> {
                Product prb = new Product(pb);
                products.add(prb);
            });
        }
        if (searchProductDTO.getPriceIni() > 0 && searchProductDTO.getPriceFin() > 0) {
            List<Product> lstProdBrand = productRepository.findByPriceBetween(searchProductDTO.getPriceIni()
                    , searchProductDTO.getPriceFin());
            lstProdBrand.forEach(pb -> {
                Product prb = new Product(pb);
                products.add(prb);
            });
        }
        // products.stream().distinct().collect(Collectors.toList());
        if (products.size() > 0) {
            Map<Long, Product> mapProducts = new HashMap<>();
            products.forEach(p -> mapProducts.put(p.getId(), p));
            mapProducts.forEach((k, v) ->
                    productsClear.add(v)
            );
        }
        return productsClear;
    }

    /**
     * Method consult all products added to shopping car
     * @param nameUser
     * @param snSale
     * @return List<ProductAddedCarDTO> all Products about advanced search
     * */
    @Override
    public List<ProductAddedCarDTO> findCarAdded(String nameUser, String snSale) {
        List<CarSale> lstCarSale = carSaleRepository.findBySnSaleAndNameUser(snSale, nameUser);
        List<ProductAddedCarDTO> lstProducts = new ArrayList<>();
        if (lstCarSale != null) {
            if (lstCarSale.size() > 0) {
                lstCarSale.forEach(c -> {
                    Product product = productRepository.findById(c.getIdProduct()).orElse(null);
                    if (product != null) {
                        ProductAddedCarDTO productAdded = new ProductAddedCarDTO(product.getId(), product.getName(),
                                product.getBrand(), c.getCreate_at(), c.getTotalPrice(), c.getQuantity());
                        lstProducts.add(productAdded);
                    }

                });
            }
        }
        return lstProducts;
    }
}
