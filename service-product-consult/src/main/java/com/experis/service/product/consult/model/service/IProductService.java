package com.experis.service.product.consult.model.service;

import com.experis.service.commons.model.dto.ProductAddedCarDTO;
import com.experis.service.commons.model.dto.SearchProductDTO;
import com.experis.service.commons.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Interface for job with all consults of shopping car
 * @author dpanquev
 * @version 2021-06-21
 * */
public interface IProductService {

    public Page<Product> findAll(Pageable pageable);

    public List<Product> finAll();

    public List<Product> search(SearchProductDTO searchProductDTO);

    public List<ProductAddedCarDTO> findCarAdded(String nameUser, String snSale);
}
