package com.experis.service.product.consult.controller;

import com.experis.service.commons.model.dto.ProductAddedCarDTO;
import com.experis.service.commons.model.dto.SearchProductDTO;
import com.experis.service.commons.model.entity.Product;
import com.experis.service.product.consult.model.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Class for job with all request of shopping car
 * @author dpanquev
 * @version 2021-06-21
 * */
@RestController
@RequestMapping("/VI/product")
public class ProductConsultController {

    @Autowired
    private IProductService productService;


    /**
     * Method consult products paged to shopping car
     * @param page
     * @return Page<Product> Products paged
     * */
    @GetMapping("/products/page/{page}")
    public Page<Product> index(@PathVariable Integer page) {
        return productService.findAll(PageRequest.of(page, 3));
    }

    /**
     * Method consult all products to shopping car
     * @return List<Product> all Products
     * */
    @GetMapping("/products")
    public List<Product> index() {
        return productService.finAll();
    }

    /**
     * Method consult all Products about advanced search
     * @param searchProductDTO
     * @return List<Product> all Products about advanced search
     * */
    @PostMapping("/products/search/")
    public List<Product> search(@RequestBody SearchProductDTO searchProductDTO) {
        return productService.search(searchProductDTO);
    }

    /**
     * Method consult all products added to shopping car
     * @param nameUser
     * @return List<ProductAddedCarDTO> all Products about advanced search
     * */
    @GetMapping("/products/car/added/{nameUser}")
    public List<ProductAddedCarDTO> carAdded(@PathVariable String nameUser) {
        return productService.findCarAdded(nameUser, "N");
    }


}
