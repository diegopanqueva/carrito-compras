package com.experis.service.product.load.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan({"com.experis.service.commons.model.entity"})
@EnableJpaRepositories("com.experis.service.commons.model.repository")
public class ServiceProductLoadBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceProductLoadBatchApplication.class, args);
    }

}
