package com.experis.service.product.load.batch.model.batch;

import com.experis.service.commons.model.entity.Product;
import com.experis.service.commons.model.repository.IProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class DbWriter implements ItemWriter<Product> {

    private Logger LOGGER = LoggerFactory.getLogger(DbWriter.class);

    @Autowired
    private IProductRepository productRepository;

    @Transactional
    @Override
    public void write(List<? extends Product> list) throws Exception {
        list.forEach(p -> {
            if (!p.getName().equals("") && !p.getBrand().equals("") &&
                    !p.getStatus().equals("") && p.getPrice() != null &&
                    p.getStock() != null && p.getLess() != null) {
                LOGGER.info("Product complete");
                Product existProduct = productRepository.findByNameAndBrand(p.getName(), p.getBrand());
                if (existProduct == null) {
                    productRepository.save(p);
                    LOGGER.info("Product created! " + p);
                }
            }
        });


    }
}
