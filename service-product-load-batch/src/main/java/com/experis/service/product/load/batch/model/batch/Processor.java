package com.experis.service.product.load.batch.model.batch;

import com.experis.service.commons.model.entity.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class Processor implements ItemProcessor<Product, Product> {

    private Logger LOGGER = LoggerFactory.getLogger(Processor.class);

    @Override
    public Product process(Product product) throws Exception {
        return product;
    }
}
