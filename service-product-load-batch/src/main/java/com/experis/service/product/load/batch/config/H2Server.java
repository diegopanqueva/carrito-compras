package com.experis.service.product.load.batch.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.h2.tools.Server;

import java.sql.SQLException;

/**
 * Class for job with h2 database of shopping car
 * @author dpanquev
 * @version 2021-06-21
 * */
@Configuration
public class H2Server {

    /**
     * Method for expose port tcp and connect to database
     * @return Server
     * */
    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server inMemoryH2DatabaseServer() throws SQLException {
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "7800");
    }

    /**
     * Method for expose port tcp and connect to database
     * @return Server
     * */
    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server inMemoryH2DatabaseServer1() throws SQLException {
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "7801");
    }
}
