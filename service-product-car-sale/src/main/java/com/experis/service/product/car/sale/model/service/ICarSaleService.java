package com.experis.service.product.car.sale.model.service;

import com.experis.service.commons.model.dto.CarSaleDTO;
import com.experis.service.product.car.sale.model.dto.ResponseDTO;

/**
 * Interface for job with all request of shopping car
 * @author dpanquev
 * @version 2021-06-21
 * */
public interface ICarSaleService {

    public ResponseDTO adminCar(CarSaleDTO carSaleDTO);

    public ResponseDTO deleteAddedCar(Long id, String snSale, String nameUser);

    public ResponseDTO deleteAllCar(String nameUser);
}
