package com.experis.service.product.car.sale.model.service;


import com.experis.service.commons.model.dto.CarSaleDTO;
import com.experis.service.product.car.sale.model.dto.ResponseDTO;
import com.experis.service.commons.model.entity.CarSale;
import com.experis.service.commons.model.entity.Product;
import com.experis.service.commons.model.repository.ICarSaleRepository;
import com.experis.service.commons.model.repository.IProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
/**
 * Class for job with all admin whit the shopping car
 * @author dpanquev
 * @version 2021-06-21
 * */
@Service
public class CarSaleServiceImpl implements ICarSaleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarSaleServiceImpl.class);

    @Autowired
    private ICarSaleRepository carSaleRepository;

    @Autowired
    private IProductRepository productRepository;

    /**
     * Method for delete and create product to shopping car
     * @param carSaleDTO
     * @return ResponseDTO state about request transaction
     * */
    @Override
    public ResponseDTO adminCar(CarSaleDTO carSaleDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        Product stock = productRepository.findById(carSaleDTO.getIdProduct()).orElse(null);
        CarSale carSaleActual = carSaleRepository.findByIdProductAndSnSaleAndNameUser(carSaleDTO.getIdProduct(), "N", carSaleDTO.getNameUser());
        if (carSaleDTO.getSnSale().equalsIgnoreCase("N")) {
            // verify stock and add car sale
            if (carSaleActual != null) {
                Long quantity = carSaleDTO.getQuantity() + carSaleActual.getQuantity();
                carSaleActual.setQuantity(quantity);
                carSaleDTO.setQuantity(quantity);
                carSaleDTO.setId(carSaleActual.getId());
            }
            if (stock != null) {
                if (stock.getStock() > carSaleDTO.getQuantity()) {
                    CarSale carSale = new CarSale(carSaleDTO);
                    carSaleRepository.save(carSale);
                    responseDTO.setMessage("Product added");
                    responseDTO.setStatus(HttpStatus.CREATED);
                } else {
                    responseDTO.setMessage("Stock not allowed, exceeds the maximum of stocks");
                    responseDTO.setStatus(HttpStatus.BAD_REQUEST);
                }
            }else{
                responseDTO.setMessage("The Products not exists");
                responseDTO.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else if (carSaleDTO.getSnSale().equalsIgnoreCase("S") && carSaleActual != null) {
            // update stock
            Integer stockUpdated = stock.getStock() - carSaleActual.getQuantity().intValue();
            stock.setStock(stockUpdated);
            productRepository.save(stock);
            // update car added
            CarSale carSale = new CarSale(carSaleDTO);
            carSale.setQuantity(carSaleActual.getQuantity());
            carSale.setTotalPrice(carSaleActual.getQuantity() * carSaleActual.getUnitPrice());
            carSale.setCreate_at(new Date());
            carSale.setId(carSaleActual.getId());
            carSaleRepository.save(carSale);
            responseDTO.setMessage("Paid product");
            responseDTO.setStatus(HttpStatus.CREATED);
        } else {
            responseDTO.setMessage("Stock not allowed, you have no added products");
            responseDTO.setStatus(HttpStatus.BAD_REQUEST);
        }
        return responseDTO;
    }

    /**
     * Method for delete product to shopping car
     * @param id
     * @param snSale
     * @param nameUser
     * @return ResponseDTO state about request transaction
     * */
    @Override
    public ResponseDTO deleteAddedCar(Long id, String snSale, String nameUser) {
        ResponseDTO responseDTO = new ResponseDTO();
        if (snSale.equalsIgnoreCase("N")) {
            CarSale carSaleActual = carSaleRepository.findByIdProductAndSnSaleAndNameUser(id, snSale, nameUser);
            if (carSaleActual != null) {
                carSaleRepository.deleteById(carSaleActual.getId());
                responseDTO.setMessage("product removed from cart successful");
                responseDTO.setStatus(HttpStatus.OK);
            } else {
                responseDTO.setMessage("Product not exist");
                responseDTO.setStatus(HttpStatus.NOT_FOUND);
            }
        } else {
            responseDTO.setMessage("Stock not allowed, you have no added products");
            responseDTO.setStatus(HttpStatus.BAD_REQUEST);
        }
        return responseDTO;

    }

    /**
     * Method for delete all products to shopping car
     * @param nameUser
     * @return ResponseDTO state about request transaction
     * */
    @Override
    public ResponseDTO deleteAllCar(String nameUser) {
        ResponseDTO responseDTO = new ResponseDTO();
        List<CarSale> lstCarSaleAssign = carSaleRepository.findBySnSaleAndNameUser("N", nameUser);
        if (lstCarSaleAssign != null) {
            if (lstCarSaleAssign.size() > 0) {
                lstCarSaleAssign.forEach(c -> carSaleRepository.deleteById(c.getId()));
                responseDTO.setMessage("Deleted all successful");
                responseDTO.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseDTO.setMessage("Products assigned");
                responseDTO.setStatus(HttpStatus.NOT_FOUND);
            }
        } else {
            responseDTO.setMessage("Products not exists");
            responseDTO.setStatus(HttpStatus.NOT_FOUND);
        }
        return responseDTO;
    }
}
