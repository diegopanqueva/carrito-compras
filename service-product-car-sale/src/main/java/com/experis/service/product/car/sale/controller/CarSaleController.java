package com.experis.service.product.car.sale.controller;

import com.experis.service.commons.model.dto.CarSaleDTO;
import com.experis.service.product.car.sale.model.dto.ResponseDTO;
import com.experis.service.product.car.sale.model.service.ICarSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for job with all request of shopping car
 * @author dpanquev
 * @version 2021-06-21
 * */
@RestController
@RequestMapping("/VI/product")
public class CarSaleController {

    @Autowired
    private ICarSaleService carSaleService;

    /**
     * Method for create add product to shopping car
     * @param carSaleDTO
     * @return ResponseEntity<> state about request transaction
     * */
    @PostMapping("products/addCar")
    public ResponseEntity<?> addCar(@RequestBody CarSaleDTO carSaleDTO) {
        Map<String, Object> response = new HashMap<>();
        carSaleDTO.setSnSale("N");
        ResponseDTO carAdded = carSaleService.adminCar(carSaleDTO);
        response.put("message", "Add successful");
        response.put("added", carAdded.getMessage());
        return new ResponseEntity<Map<String, Object>>(response, carAdded.getStatus());
    }

    /**
     * Method for create pay product to shopping car
     * @param carSaleDTO
     * @return ResponseEntity<> state about request transaction
     * */
    @PostMapping("products/payCar")
    public ResponseEntity<?> payCar(@RequestBody CarSaleDTO carSaleDTO) {
        Map<String, Object> response = new HashMap<>();
        carSaleDTO.setSnSale("S");
        ResponseDTO carPay = carSaleService.adminCar(carSaleDTO);
        response.put("message", "Paid successful");
        response.put("paid", carPay.getMessage());
        return new ResponseEntity<Map<String, Object>>(response, carPay.getStatus());
    }

    /**
     * Method for delete product to shopping car
     * @param carSaleDTO
     * @return ResponseEntity<> state about request transaction
     * */
    @DeleteMapping("products/deleteCar")
    public ResponseEntity<?> deleteCar(@RequestBody CarSaleDTO carSaleDTO) {
        Map<String, Object> response = new HashMap<>();
        carSaleDTO.setSnSale("N");
        ResponseDTO carPay = carSaleService.deleteAddedCar(carSaleDTO.getIdProduct(),carSaleDTO.getSnSale()
                ,carSaleDTO.getNameUser());
        response.put("message", "Delete successful");
        response.put("Delete", carPay.getMessage());
        return new ResponseEntity<Map<String, Object>>(response, carPay.getStatus());
    }

    /**
     * Method for delete all products to shopping car
     * @param nameUser
     * @return ResponseEntity<> state about request transaction
     * */
    @DeleteMapping("products/deleteCar/all/{nameUser}")
    public ResponseEntity<?> deleteCarAll(@PathVariable String nameUser) {
        Map<String, Object> response = new HashMap<>();
        ResponseDTO carPay = carSaleService.deleteAllCar(nameUser);
        response.put("message", "Paid successful");
        response.put("paid", carPay.getMessage());
        return new ResponseEntity<Map<String, Object>>(response, carPay.getStatus());
    }


}
